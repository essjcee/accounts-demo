package com.example.demo;

public class UseAccounts {
	/*
	 * Kahoot question and answers
	 * A BankAccount class and a SavingsAccount class that extends BankAccount, 
	 * instances ba and sa of each. What is valid?
	 * sa = ba;
	 * ba = sa; Correct Answer
	 * sa = new BankAccount();
	 * sa = (BankAccount) ba;
	 */
	public static void main(String[] args) {
		BankAccount ba = new BankAccount("Fred Smith",500);
		System.out.println(ba);
		SavingsAccount sa = new SavingsAccount("Susan Jess", 200);
		System.out.println(sa);
		// sa = ba;
		// ba = sa; //allowed
		// sa = new BankAccount()
		// sa = (BankAccount)ba;
		// System.out.println(ba);
		BankAccount recipient = new BankAccount("Alice O'Neill", 600);
		transfer(sa,recipient,100);
		System.out.println(sa);
	}
	
	static void transfer(BankAccount from, BankAccount to, double amount) {
		if(from instanceof SavingsAccount) { 
			if(from.getBalance() - amount < 0) {
				System.out.println("Insufficient funds to transfer");
			}
			else {
				from.withdraw(amount);
				to.deposit(amount);
			}
		}
		else {
			from.withdraw(amount);
			to.deposit(amount);
		}
	}
} 
 
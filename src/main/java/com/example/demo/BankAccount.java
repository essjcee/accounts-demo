package com.example.demo;

public class BankAccount {
	private String name;
	protected double balance;
	public BankAccount(String name, double balance) {
		super();
		this.name = name;
		this.balance = balance;
	}
	public BankAccount() {
		this("Anonymous", 100);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void withdraw(double amount) {
		balance -= amount;
	}
	
	public void deposit(double amount) {
		balance += amount;
	}
	
	public double getBalance() {
		return balance;
	}
	
	@Override
	public String toString() {
		return String.format("Account %s has balance %.2f", name, balance);
	}
}

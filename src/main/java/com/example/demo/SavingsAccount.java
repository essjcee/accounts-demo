package com.example.demo;

public class SavingsAccount extends BankAccount{
	private static double interestRate = 0.05;
	public SavingsAccount(String name, double balance) {
		super(name, balance);
	}
	
	public static void setInterest(double interestRate) {
		SavingsAccount.interestRate = interestRate;
	}
	
	@Override
	public void withdraw(double amount) {
		if(balance - amount > 0) {
			balance -= amount;
		}
		else {
			System.out.println("Insufficient funds.");
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + String.format(" interest rate - %.2f%%", interestRate * 100);
	}
}
